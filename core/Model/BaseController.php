<?php

namespace Core\Model;


use Core\Container;

class BaseController
{
    public function renderJson($result)
    {
        header('Content-Type: application/json');
        echo json_encode($result);
        exit(0);
    }

    public function renderSuccessJson()
    {
        $stdClass = new \stdClass();
        $stdClass->success = true;
        $this->renderJson($stdClass);
    }

    public function getRepository($name)
    {
        return Container::getRepository($name);
    }

    public function get($name)
    {
        return Container::get($name);
    }

    public function addDataAttribute($result)
    {
        $stdClass = new \stdClass();
        $stdClass->data = $result;
        return $stdClass;
    }

}