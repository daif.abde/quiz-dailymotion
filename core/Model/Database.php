<?php

namespace Core\Model;


use Symfony\Component\Yaml\Yaml;

use \PDO;

class Database
{
    protected static $_instance;
    private $pdo;

    /**
     * Database constructor.
     * @param $dbName
     * @param $dbUser
     * @param $dbPass
     * @param $dbHost
     */
    public function __construct($dbName, $dbUser, $dbPass, $dbHost)
    {
        $pdo = new PDO('mysql:host='.$dbHost.';port=3306;dbname='.$dbName,$dbUser,$dbPass);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->pdo = $pdo;
    }

    /**
     * @return Database
     */
    public static function getInstance()
    {
        if (empty(self::$_instance)) {
            $config = Yaml::parseFile(APP_FOLDER . '/config/global.yml');
            $dbConfig = $config['database'];
            self::$_instance = new Database($dbConfig['name'], $dbConfig['user'], $dbConfig['password'], $dbConfig['host']);
        }
        return self::$_instance;
    }

    public function getPDO()
    {
        return $this->pdo;
    }
}