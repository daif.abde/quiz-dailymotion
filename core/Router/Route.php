<?php

namespace Core\Router;


use Core\Exception\RouterException;

class Route
{
    private $name;
    private $path;
    private $action;
    private $matches;

    public function __construct($name, $path, $action)
    {
        $this->name = $name;
        $this->path = trim($path, '/');
        $this->action = $action;
    }

    public function match($url)
    {
        $url = trim($url, '/');
        $path = preg_replace('#:([\w]+)#', '([^/]+)', $this->path);
        $regexRoute = "#^$path$#i";
        if(!preg_match($regexRoute, $url, $matches)) {
            return false;
        }
        array_shift($matches);
        $this->matches = $matches;
        return true;
    }

    public function call()
    {
        // Extract ControllerName and action name
        $params = explode(':', $this->action);
        $controllerName = ucfirst($params[0].'Controller');
        $actionName = $params[1].'Action';

        // check if Controller file exist
        $pathController = APP_FOLDER . "/src/Controller/$controllerName.php";
        if (!file_exists($pathController))
            throw new RouterException("Controller [$pathController] Not Found");


        // init Controller object
        $controller = "App\\Controller\\$controllerName";
        $controller = new $controller();

        //Check if action exist
        if (!method_exists($controller, $actionName))
            throw new RouterException("Controller [$controllerName] has no method called [$actionName]");


        return call_user_func_array([$controller, $actionName], $this->matches);

    }
}