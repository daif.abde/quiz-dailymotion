<?php

namespace Core\Router;
use Core\Exception\RouterException;
use Symfony\Component\Yaml\Yaml;


class Router
{

    /**
     * @var string
     */
    private $url;

    /**
     * @var array
     */
    private $routes = [];

    /**
     * Register router function
     */
    public static function register()
    {
        $router = new Router($_GET['url']);
        $router->loadRoutes();
        $router->build();
    }

    /**
     * Router constructor.
     * @param $url string
     */
    public function __construct($url)
    {
        $this->url = $url;
    }

    /**
     * @param $name
     * @param $path
     * @param $action
     */
    public function get($name, $path, $action)
    {
        $this->add('GET', $name, $path, $action);
    }

    /**
     * @param $name string
     * @param $path
     * @param $action
     */
    public function post($name, $path, $action)
    {
        $this->add('POST', $name, $path, $action);
    }

    /**
     * @param $name
     * @param $method
     * @param $path
     * @param $action
     */
    public function add($name, $method, $path, $action)
    {
        $this->routes[$method][] = new Route($name, $path, $action);
    }

    public function build()
    {
        if(!isset($this->routes[$_SERVER['REQUEST_METHOD']])) {
            throw new RouterException('Method not exist');
        }
        /** @var Route $route */
        foreach ($this->routes[$_SERVER['REQUEST_METHOD']] as $route) {
            if($route->match($this->url)) {
                return $route->call();
            }
        }
        throw  new RouterException('No route matches');
    }

    public function loadRoutes()
    {
        $routes = Yaml::parseFile(APP_FOLDER."/config/routes.yml");
        foreach ($routes as $name => $route) {
            $this->add($name, $route['method'], $route['path'], $route['action']);
        }
    }

}