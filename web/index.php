<?php
require '../vendor/autoload.php';

define('APP_FOLDER', dirname(__FILE__) . "/..");

use Core\Router\Router;
use Core\Container;

Container::register();
Router::register();
die;
