<?php
if (preg_match('/\.(?:png|jpg|jpeg|gif)$/', $_SERVER["REQUEST_URI"])) {
    return false;
} else {
    // When using CLI php set the url param
    $_GET['url'] = $_SERVER["REQUEST_URI"];
    include __DIR__ . '/index.php';
}