# Homework <> Dailymotion

## Mission 
Building a playlist api

## Timing
Indicative estimated duration : 3h00

## Who is in charge ?
- DAIF Abderrahamn


## How to install
### Requirements
- PHP7
- composer
- mysql
- postman
- ? docker ___( see NOTE )___

### Config Files
- Change the __config/global.yml__ to match your database configuration

    ```
      host: 'localhost'
      name: 'dailymotion'
      user: 'root'
      password: ''
    ```

### Database
- Create database called __dailymotion__
- Execute the __doc/db.sql__ to create tables


### Install the system
On first install run this command:
```shell
composer install
cd web
php -S localhost:8080 router.php
```

# Consume API
- install postman
- import __postman/dailymotion.postman_collection.json__

# Note [MISSING]
- Handling error response json (at the moment return the Exception)
- Building docker (I have some issues with windows when mounting nginx config file)