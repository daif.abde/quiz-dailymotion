-- -----------------------------------------------------
-- Schema dailymotion
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `dailymotion` DEFAULT CHARACTER SET utf8 ;
USE `dailymotion` ;

-- -----------------------------------------------------
-- Table `dailymotion`.`playlist`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dailymotion`.`playlist` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dailymotion`.`video`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dailymotion`.`video` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(45) NULL,
  `thumbnail` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dailymotion`.`playlist_video`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dailymotion`.`playlist_video` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `position` INT NULL,
  `playlist_id` INT NOT NULL,
  `video_id` INT NOT NULL,
  PRIMARY KEY (`id`, `playlist_id`, `video_id`),
  INDEX `fk_playlist_video_playlist_idx` (`playlist_id` ASC),
  INDEX `fk_playlist_video_video1_idx` (`video_id` ASC),
  CONSTRAINT `fk_playlist_video_playlist`
    FOREIGN KEY (`playlist_id`)
    REFERENCES `dailymotion`.`playlist` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_playlist_video_video1`
    FOREIGN KEY (`video_id`)
    REFERENCES `dailymotion`.`video` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



-- -----------------------------------------------------
-- INSERT DATA
-- -----------------------------------------------------


-- -----------------------------------------------------
-- playlist DATA
-- -----------------------------------------------------
INSERT INTO `playlist` (`id`, `name`) VALUES
(1, 'playlist 1 name'),
(2, 'playlist 2 name'),
(3, 'playlist 3 name'),
(4, 'playlist 4 name');


-- -----------------------------------------------------
-- video DATA
-- -----------------------------------------------------
INSERT INTO `video` (`id`, `title`, `thumbnail`) VALUES
(1, 'Video title 1', 'Thumbnail title 1'),
(2, 'Video title 2', 'Thumbnail title 1'),
(3, 'Video title 3', 'Thumbnail title 3'),
(4, 'Video title 4', 'Thumbnail title 4');


-- -----------------------------------------------------
-- playlist_video DATA
-- -----------------------------------------------------
INSERT INTO `playlist_video` (`id`, `position`, `playlist_id`, `video_id`) VALUES
(1, 1, 1, 1),
(2, 2, 1, 2),
(3, 3, 1, 3),
(4, 4, 1, 4),
(5, 1, 2, 1),
(6, 2, 2, 2),
(7, 3, 2, 3),
(8, 4, 2, 4),
(9, 1, 3, 1),
(10, 2, 3, 2),
(11, 3, 3, 3),
(12, 4, 3, 4),
(13, 1, 4, 1),
(14, 2, 4, 2),
(15, 3, 4, 3),
(16, 4, 4, 4);