<?php

namespace App\Repository;


use App\Entity\PlaylistVideo;
use Core\Model\BaseRepository;

class PlaylistVideoRepository extends BaseRepository
{

    /**
     * @param $id
     * @return array
     */
    public function findByPlaylistId($id)
    {
        $res = $this->query("SELECT * FROM $this->tableName WHERE playlist_id = $id ORDER BY `position` ASC");
        $entities = [];
        foreach ($res as $stdClass) {
            $entities[] = $this->mapToEntity($stdClass);
        }
        return $entities;
    }

    /**
     * map std class to entity
     * @param $stdClass
     * @return PlaylistVideo
     */
    public function mapToEntity($stdClass)
    {
        $playlistVideo = new PlaylistVideo();
        $playlistVideo->id = $stdClass->id;
        $playlistVideo->position = $stdClass->position;
        $playlistVideo->video = $playlistVideo->getVideo($stdClass->video_id);
        $playlistVideo->playlist = $playlistVideo->getPlaylist($stdClass->playlist_id);
        return $playlistVideo;
    }


    /**
     * Add video to a playlist
     * @param $idPlaylist
     * @param $idVideo
     * @return bool
     */
    public function addVideoToPlaylist($idPlaylist, $idVideo)
    {
        $position = $this->getNextPosition();
        $sql = 'INSERT INTO playlist_video ( `position`, playlist_id, video_id) VALUES (:position, :playlist_id, :video_id)';
        $sth = $this->getPDO()->prepare($sql);
        $params[':video_id'] = $idVideo;
        $params[':position'] = $position;
        $params[':playlist_id'] = $idPlaylist;
        $code = $sth->execute($params);
        return $code;
    }

    /**
     * get the next position to add video
     * @return int
     */
    public function getNextPosition()
    {
        try {
            $sql = 'SELECT max(position) as position FROM playlist_video';
            $res = $this->query($sql);
            $res = array_shift($res);
            $max = $res->position;
            $next = intval($max) + 1;
            return $next;
        } catch (\Exception $e){
            return 1;
        }
    }

    /**
     * Check if video is in playlist
     * @param $idPlaylist
     * @param $idVideo
     * @return bool
     */
    public function videoExistInPlaylist($idPlaylist, $idVideo)
    {
        return !!$this->findByPlaylistAndVideo($idPlaylist,$idVideo);
    }

    /**
     * Delete video from playlist
     * @param $idPlaylist
     * @param $idVideo
     * @return bool
     */
    public function deleteVideoFromPlaylist($idPlaylist, $idVideo)
    {
        $playlistVideo = $this->findByPlaylistAndVideo($idPlaylist, $idVideo);
        if(! $playlistVideo) return false;

        $sqlDelete = 'DELETE FROM `playlist_video` WHERE id = :id ';
        $qr = $this->getPDO()->prepare($sqlDelete);
        $codeDelete = $qr->execute([':id' => $playlistVideo->getId()]);
        if(!$codeDelete) return false;
        return $this->deletePosition($playlistVideo->getPosition());
    }

    /**
     * Find Playlist by playlist id and video id
     * @param $idPlaylist
     * @param $idVideo
     * @return PlaylistVideo|null
     */
    public function findByPlaylistAndVideo($idPlaylist, $idVideo)
    {
        $sql = 'SELECT * FROM playlist_video WHERE playlist_id = :playlist_id AND video_id = :video_id';
        $qr = $this->getPDO()->prepare($sql);
        $qr->execute([':playlist_id' => $idPlaylist, ':video_id' => $idVideo]);
        $res = $qr->fetchAll(\PDO::FETCH_OBJ);
        $res =  array_shift($res);
        if(!$res) return null;
        return PlaylistVideo::mapData($res);
    }

    /**
     * Change the videos existing position
     * @param $position
     * @return bool
     */
    public function deletePosition($position)
    {
        $sql = 'UPDATE playlist_video SET position = position -1 WHERE position > :cp';
        $qr = $this->getPDO()->prepare($sql);
        return $qr->execute([':cp' => $position]);
    }

}