<?php

namespace App\Controller;


use App\Repository\PlaylistRepository;
use App\Repository\PlaylistVideoRepository;
use App\Service\PlaylistService;
use Core\Exception\ContainerException;
use Core\Exception\RouterException;
use Core\Model\BaseController;

class PlaylistController extends BaseController
{
    /**
     * List all playlists
     */
    public function listAction()
    {
        /** @var PlaylistService $playlistService */
        $playlistService = $this->get('playlist_service');
        $this->renderJson($this->addDataAttribute($playlistService->getList()));
    }

    /**
     * Show specific playlist
     * @param $id
     */
    public function showAction($id)
    {
        /** @var PlaylistService $playlistService */
        $playlistService = $this->get('playlist_service');
        $this->renderJson($this->addDataAttribute($playlistService->get($id)));
    }

    /**
     * List all video of playlist
     * @param $idPlaylist
     */
    public function listVideosAction($idPlaylist)
    {
        /** @var PlaylistService $playlistService */
        $playlistService = $this->get('playlist_service');
        $videos = $playlistService->getVideos($idPlaylist);
        $this->renderJson($this->addDataAttribute($videos));
    }

    /**
     * Create playlist
     * @throws ContainerException
     */
    public function createAction()
    {
        $name = $_POST['name'];
        /** @var PlaylistService $playlistService */
        $playlistService = $this->get('playlist_service');
        $code = $playlistService->create($name);
        if(!$code) {
            throw  new ContainerException('ERROR DATABASE');
        }
        $this->renderSuccessJson();
    }

    /**
     * Delete playlist
     * @param $id
     * @throws ContainerException
     */
    public function deleteAction($id)
    {
        /** @var PlaylistRepository $playlistRepo */
        $playlistRepo = $this->getRepository('playlist');
        $code = $playlistRepo->delete($id);
        if(!$code) {
            throw new ContainerException('DATABASE ERROR');
        }

        $this->renderSuccessJson();
    }

    /**
     * update playlist
     * @param $id
     * @throws RouterException
     */
    public function updateAction($id)
    {
        //fetch post vars from PUT request
        parse_str(file_get_contents("php://input"), $postVars);

        if(!isset($postVars['name'])){
            throw new RouterException('Missing Post param [name]');
        }
        $name = $postVars['name'];
        /** @var PlaylistService $playlistService */
        $playlistService = $this->get('playlist_service');
        $playlistService->update($id, $name);
        
        $this->renderSuccessJson();
    }

    /**
     * Add video to playlist
     * @param $idPlaylist
     * @throws ContainerException
     */
    public function addVideoToPlaylistAction($idPlaylist)
    {
        $idVideo = $_POST['video_id'];
        // check if Video already in playlist
        /** @var PlaylistVideoRepository $repoPV */
        $repoPV = $this->getRepository('playlist_video');
        if($repoPV->videoExistInPlaylist($idPlaylist, $idVideo)) {
            throw new ContainerException('Video Already exist in playlist');
        }
        // add video to playlist
        /** @var PlaylistService $playlistService */
        $playlistService = $this->get('playlist_service');
        $code = $playlistService->addVideoToPlaylist($idPlaylist, $idVideo);

        if(!$code) {
            throw  new ContainerException('ERROR DATABASE');
        }
        $this->renderSuccessJson();

    }

    /**
     * Remove video from playlist
     * @param $idPlaylist
     * @param $idVideo
     * @throws ContainerException
     */
    public function removeVideoFromPlaylistAction($idPlaylist, $idVideo)
    {
        /** @var PlaylistVideoRepository $repoPlaylistVideo */
        $repoPlaylistVideo = $this->getRepository('playlist_video');
        $result = $repoPlaylistVideo->deleteVideoFromPlaylist($idPlaylist, $idVideo);
        if(!$result) {
            throw new ContainerException('ERROR DATABASE');
        }
        $this->renderSuccessJson();
    }
}