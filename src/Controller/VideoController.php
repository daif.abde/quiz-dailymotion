<?php

namespace App\Controller;


use App\Service\VideoService;
use Core\Model\BaseController;

class VideoController extends BaseController
{
    /**
     * List all videos
     */
    public function listAction()
    {
        /** @var VideoService $videoService */
        $videoService = $this->get('video_service');
        $this->renderJson($this->addDataAttribute($videoService->getList()));
    }

    /**
     * Show video
     * @param $id
     */
    public function showAction($id)
    {
        /** @var VideoService $videoService */
        $videoService = $this->get('video_service');
        $this->renderJson($this->addDataAttribute($videoService->get($id)));
    }
}