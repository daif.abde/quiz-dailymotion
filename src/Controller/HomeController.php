<?php

namespace App\Controller;

use Core\Model\BaseController;


class HomeController extends BaseController
{
    /**
     * Home page
     */
    public function indexAction()
    {
        ?>
        <h1>Homework <> Dailymotion</h1>
        <p><span style="color: #333333;">Building a playlist api<br/>Indicative estimated duration : 3h00<br/><br/>Introduction<br/><br/>Dailymotion is building a new feature "The playlist"<br/><br/>The feature is simple : The user can create a list of ordered videos.<br/><br/>As a core api developer, you are responsible for building this feature and expose it through API.<br/><br/>Task<br/>The task is to create an api that manages an ordered playlist.<br/>An example of a minimal video model : (You might add extra fields to do this project)<br/><br/>video {<br/>id : the id of the video,<br/>title: the title of the video<br/>thumbnail : The url of the video<br/>...<br/>}<br/><br/>An example of a minimal playlist model : (You might add extra fields to do this project)<br/><br/>playlist {<br/>id : The id of the playlist,<br/>name : The name of the playlist<br/>......<br/>}<br/><br/>The API must support the following use cases:<br/><br/>- Return the list of all videos:<br/><br/>{<br/>"data" : [<br/>{<br/>"id": 1,<br/>"title": "video 1"<br/>....<br/>},<br/>{<br/>"id": 2,<br/>"title": "video 2"<br/>}<br/>....<br/>]<br/>}<br/><br/>- Return the list of all playlists:<br/><br/>{<br/>"data" : [<br/>{<br/>"id": 1,<br/>"name": "playlist 1"<br/>....<br/>},<br/>{<br/>"id": 2,<br/>"name": "playlist 2"<br/>}<br/>&hellip;.<br/><br/>]<br/>}<br/><br/>- Create a playlist<br/><br/>- Show informations about the playlist<br/><br/>{<br/>"data" : {<br/>"id": 1,<br/>"name": "playlist 1"<br/>}<br/>}<br/><br/>- Update informations about the playlist<br/>- Delete the playlist<br/>- Add a video in a playlist<br/>- Delete a video from a playlist<br/>- Return the list of all videos from a playlist (ordered by position):<br/><br/>{<br/>"data" : [<br/>{<br/>"id": 1,<br/>"title": "video 1 from playlist 2"<br/>....<br/>},<br/>{<br/>"id": 2,<br/>"title": "video 2 from playlist 2"<br/>}<br/>....<br/>]<br/>}<br/><br/>Your goal: Design and build this API.<br/><br/>Important notes :<br/>- Removing videos should re-arrange the order of your playlist and the storage.<br/>- PHP or Python languages are supported<br/>- Using frameworks is forbidden, your code should use native language libraries, except for Python, you could use bottlepy (&nbsp;</span><a>https://bottlepy.org/docs/dev/</a><span
                    style="color: #333333;">).<br/>- Use Mysql for storing your data<br/>- You should provide us the source code (or a link to GitHub) and the instructions to run your code</span>
        </p>
        <?php
    }
}