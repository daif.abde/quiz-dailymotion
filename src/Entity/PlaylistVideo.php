<?php

namespace App\Entity;


use Core\Container;

class PlaylistVideo
{

    /** @var  integer */
    public $id;

    /** @var  integer */
    public $playlistId;

    /** @var  integer */
    public $videoId;

    /** @var  integer */
    public $position;

    /** @var  Video */
    public $video;

    /** @var  Playlist */
    public $playlist;

    public function getPlaylist($id)
    {
        return Container::getRepository('playlist')->find($id);
    }

    public function getVideo($id)
    {
        return Container::getRepository('video')->find($id);
    }

    /**
     * @param $id integer
     */
    public function setPlaylistId($id)
    {
        $this->playlistId = intval($id);
    }

    /**
     * @param $id integer
     */
    public function setVideoId($id)
    {
        $this->videoId = intval($id);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = intval($id);
    }

    /**
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param int $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * format data to stdClass
     * @return \stdClass
     */
    public function formatData()
    {
        $stdClass = new \stdClass();
        $stdClass->id = intval($this->video->id);
        $stdClass->title = $this->video->title;
        $stdClass->position = intval($this->position);
        return $stdClass;
    }

    /**
     * map std class to PlaylistVideo class
     * @param $res
     * @return PlaylistVideo
     */
    public static function mapData($res)
    {
        $pv = new PlaylistVideo();
        $pv->setId(intval($res->id));
        $pv->setPlaylistId(intval($res->playlist_id));
        $pv->setVideoId(intval($res->video_id));
        $pv->setPosition(intval($res->position));
        return $pv;
    }

}